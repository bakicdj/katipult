<?php

namespace Race\Track;

use Exception;
use InvalidArgumentException;

/**
 * Class Car
 *
 * @package Race\Track
 */
class Car
{
    const SPEED_TOTAL = 22;
    const SPEED_MIN = 4;

    /**
     * @var array
     */
    private array $speeds = [];

    /**
     * @var int
     */
    private int $position = 1;

    /**
     * @var string
     */
    private string $name;

    /**
     * Car constructor.
     *
     * @param array $speeds
     * @param int $position
     * @param string $name
     */
    public function __construct(array $speeds, int $position, string $name)
    {
        $this->speeds   = $speeds;
        $this->position = $position;
        $this->name     = $name;
    }

    /**
     * @param Track $track
     *
     * @throws Exception
     */
    public function race(Track $track): void
    {
        $currentSegment = $this->getSegment($track, $this->getPosition());

        if ($this->getPosition() === $currentSegment->getLastElement()) {
            $positionIncreased = $this->getPosition() + 1;
            $currentSegment = $this->getSegment($track, $positionIncreased);
        }

        $amountOfElementsToPass = $this->getSpeedForSegmentType(
            $currentSegment->getType()
        );

        $newPosition = $this->getPosition() + $amountOfElementsToPass;


        $position = $newPosition > $currentSegment->getLastElement()
            ? $currentSegment->getLastElement()
            : $newPosition;

        $this->setPosition($position);
    }

    /**
     * @param Track $track
     *
     * @return bool
     * @throws Exception
     */
    public function hasWon(Track $track)
    {
        return $this->getPosition() === $track->getLastSegment()->getLastElement();
    }

    /**
     * @param Track $track
     *
     * @param int $position
     *
     * @return TrackSegment
     *
     * @throws Exception
     */
    public function getSegment(Track $track, int $position): TrackSegment
    {
        foreach ($track->getSegments() as $segment) {
            if ($this->isInSegment($segment, $position)) {
                return $segment;
            }
        }

        throw new Exception('Cannot get car segment');
    }

    /**
     * @param TrackSegment $trackSegment
     *
     * @param int $position
     *
     * @return bool
     */
    public function isInSegment(TrackSegment $trackSegment, int $position): bool
    {
        return in_array($position, $trackSegment->getElements());
    }

    /**
     * @param string $name
     * @param int $position
     *
     * @return Car
     */
    public static function createWithRandomSpeeds(string $name, int $position = 0): Car
    {
        $speedMax = self::SPEED_TOTAL - self::SPEED_MIN;

        $speeds[TrackSegment::TYPE_STRAIGHT] = rand(self::SPEED_MIN, $speedMax);
        $speeds[TrackSegment::TYPE_CURVE]    = self::SPEED_TOTAL - $speeds[TrackSegment::TYPE_STRAIGHT];

        return new static($speeds, $position, $name);
    }

    /**
     * @param int $elementType
     *
     * @return int
     */
    public function getSpeedForSegmentType(int $elementType): int
    {
        if (!isset($this->speeds[$elementType])) {
            throw new InvalidArgumentException('Invalid element type');
        }

        return $this->speeds[$elementType];
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'position' => $this->getPosition(),
            'name'     => $this->getName()
        ];
    }
}