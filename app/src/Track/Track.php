<?php

namespace Race\Track;


/**
 * Class Track
 *
 * @package Race\Track
 */
class Track
{
    /**
     * @var TrackSegment[]
     */
    private array $segments = [];

    /**
     * Track constructor.
     *
     * @param array $trackSegments
     */
    public function __construct(array $trackSegments)
    {
        $this->segments = $trackSegments;
    }

    /**
     * @return TrackSegment[]
     */
    public function getSegments(): array
    {
        return $this->segments;
    }

    /**
     * @return TrackSegment
     */
    public function getLastSegment(): TrackSegment
    {
        $segments = $this->getSegments();

        return end($segments);
    }

    /**
     * @return array
     */
    public function getSegmentedElements(): array
    {
        $segmentedElements = [];
        $elements = 1;

        foreach ($this->getSegments() as $segment) {
            $segmentedElements[] = range($elements, $segment->getElementCount());
            $elements += $segment->getElementCount();
        }

        return $segmentedElements;
    }

    /**
     * @param array $trackSegments
     *
     * @return Track
     */
    public static function create(array $trackSegments)
    {
        return new static($trackSegments);
    }
}