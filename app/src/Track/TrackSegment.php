<?php

namespace Race\Track;

/**
 * Class TrackSegment
 *
 * @package Race\Track
 */
class TrackSegment
{
    const TYPE_CURVE = 0;
    const TYPE_STRAIGHT = 1;

    /**
     * @var int
     */
    private int $type;

    /**
     * @var array
     */
    private array $elements;

    /**
     * TrackSegment constructor.
     *
     * @param int $type
     * @param array $elements
     */
    public function __construct(int $type, array $elements)
    {
        $this->type     = $type;
        $this->elements = $elements;
    }

    /**
     * TrackSegment constructor.
     *
     * @param array $elements
     *
     * @return TrackSegment
     */
    public static function createWithRandomType(array $elements): TrackSegment
    {
        $segmentTypes = [
            TrackSegment::TYPE_CURVE,
            TrackSegment::TYPE_STRAIGHT
        ];

        return new static(
            array_rand($segmentTypes),
            $elements
        );
    }

    /**
     * @return int
     */
    public function getLastElement(): int
    {
        $elements = $this->getElements();

        return end($elements);
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }
}