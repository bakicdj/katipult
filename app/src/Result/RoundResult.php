<?php

namespace Race\Result;

/**
 * Class RoundResult
 *
 * @package Race\Result
 */
class RoundResult
{
    /**
     * @var int
     */
    public int $step;

    /**
     * @var array
     */
    public array $carsPosition;

    public function __construct(int $step, array $carsPosition)
    {
        $this->step         = $step;
        $this->carsPosition = $carsPosition;
    }
}
