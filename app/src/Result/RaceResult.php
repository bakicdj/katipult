<?php

namespace Race\Result;

/**
 * Class RaceResult
 *
 * @package Race\Result
 */
class RaceResult
{
    /**
     * @var array of StepResult
     */
    private array $roundResults = [];

    /**
     * @param RoundResult $roundResult
     */
    public function addRoundResult(RoundResult $roundResult): void
    {
        $this->roundResults[] = $roundResult;
    }

    /**
     * @return RoundResult[]
     */
    public function getRoundResults(): array
    {
        return $this->roundResults;
    }
}
