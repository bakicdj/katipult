<?php

namespace Race;

use Race\Result\RaceResult;
use Race\Result\RoundResult;
use Race\Track\Car;
use Race\Track\Track;
use Race\Track\TrackSegment;

/**
 * Class Race
 *
 * @package Race
 */
class Race
{
    const ELEMENTS_COUNT = 2000;
    const ELEMENTS_PER_SEGMENT = 40;
    const MAX_STEPS = 1000;

    /**
     * @return array
     */
    private function createTrackSegments()
    {
        $segments = [];
        $elements = [];

        for ($element = 0; self::ELEMENTS_COUNT >= $element; $element++) {

            $elements[] = $element;

            if ($element % self::ELEMENTS_PER_SEGMENT === 0) {
                $segments[] = TrackSegment::createWithRandomType($elements);
                $elements   = [];
            }
        }

        return $segments;
    }

    /**
     * @return Car[]
     */
    private function createCars(): array
    {
        return [
            Car::createWithRandomSpeeds('c1'),
            Car::createWithRandomSpeeds('c2'),
            Car::createWithRandomSpeeds('c3'),
            Car::createWithRandomSpeeds('c4'),
            Car::createWithRandomSpeeds('c5'),
        ];
    }

    /**
     * @return RaceResult
     *
     * @throws \Exception
     */
    public function race(): RaceResult
    {
        $raceResult = new RaceResult();

        $track = Track::create(
            $this->createTrackSegments()
        );

        $cars = $this->createCars();

        for ($step = 1; self::MAX_STEPS > $step; $step++) {
            foreach ($cars as $car) {
                $car->race($track);

                if ($car->hasWon($track)) {

                    $raceResult->addRoundResult(
                        $this->createRoundResult($step, $cars)
                    );
                    
                    return $raceResult;
                }
            }

            $raceResult->addRoundResult(
                $this->createRoundResult($step, $cars)
            );
        }

        throw new \Exception('Max step reached and no winner is selected');
    }

    /**
     * @param int $step
     * @param array $cars
     *
     * @return RoundResult
     */
    private function createRoundResult(int $step, array $cars): RoundResult
    {
        usort($cars, function ($a, $b) {
            /** @var Car $a */
            /** @var Car $b */
            return $a->getPosition() < $b->getPosition();
        });

        $carsPositionSnapshot = array_map(function ($car) {
            /** @var  Car $car */
            return $car->toArray();
        }, $cars);

        return new RoundResult($step, $carsPositionSnapshot);
    }
}
