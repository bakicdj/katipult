<?php

use PHPUnit\Framework\TestCase;
use Race\Race;
use Race\Result\RaceResult;

/**
 * Created by Djordje Bakic <bakicdj@gmail.com>
 *
 * Date: 12/15/20
 */

class RaceTest extends TestCase
{
    public function testInstanceOfRaceResult()
    {
        $race = new Race();

        $raceResult = $race->race();

        $this->assertInstanceOf(RaceResult::class, $raceResult);
    }
}