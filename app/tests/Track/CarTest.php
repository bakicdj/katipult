<?php

use PHPUnit\Framework\TestCase;
use Race\Race;
use Race\Result\RaceResult;
use Race\Track\Car;
use Race\Track\Track;
use Race\Track\TrackSegment;

/**
 * Class CarTest
 */
class CarTest extends TestCase
{
    private Track $track;

    protected function setUp(): void
    {
        $this->track = new Track([
            new TrackSegment(TrackSegment::TYPE_STRAIGHT, range(0, 20)),
            new TrackSegment(TrackSegment::TYPE_CURVE,    range(20, 30))
        ]);

        parent::setUp();
    }

    public function testRace()
    {
        $speeds1 = [
            TrackSegment::TYPE_STRAIGHT => 15,
            TrackSegment::TYPE_CURVE => 5,
        ];

        $speeds2 = [
            TrackSegment::TYPE_STRAIGHT => 10,
            TrackSegment::TYPE_CURVE => 3,
        ];

        $car1 = new Car($speeds1, 0, 'Car1');
        $car2 = new Car($speeds2, 0, 'Car2');

        $this->assertFalse($car1->hasWon($this->track));
        $this->assertFalse($car2->hasWon($this->track));

        // First run
        $car1->race($this->track);
        $car2->race($this->track);
        $this->assertEquals(15, $car1->getPosition());
        $this->assertEquals(10, $car2->getPosition());

        // Max segment position reached
        $car1->race($this->track);
        $car2->race($this->track);
        $this->assertEquals(20, $car1->getPosition());
        $this->assertEquals(20, $car2->getPosition());

        // Moving to next segment with type curve
        $car1->race($this->track);
        $car2->race($this->track);
        $this->assertEquals(25, $car1->getPosition());
        $this->assertEquals(23, $car2->getPosition());

        // Winning the race
        $car1->race($this->track);
        $car2->race($this->track);
        $this->assertEquals(30, $car1->getPosition());
        $this->assertEquals(26, $car2->getPosition());

        $this->assertTrue($car1->hasWon($this->track));
        $this->assertFalse($car2->hasWon($this->track));
    }
}