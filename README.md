## Installation
```CD``` into project root and run ```docker-compose up -d```

## Testing
1. Attach to the php container ```docker-compose exec php zsh```
2. Copy ```phpunit.xml.dist to phpunit.xml```
3. Run ```vendor/bin/phpunit```